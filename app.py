from flask import Flask, request, make_response
from pyotp import TOTP
from os import getenv

# default is "test" in base32
otp = TOTP(getenv("TOTP_SEED", default="ORSXG5A="))

app = Flask(__name__)


@app.route("/", methods=["GET"])
def check():
    if "Authorization" not in request.headers:
        return make_response({"error": "Authorization header not found."}, 400)

    auth_token = request.headers["Authorization"]
    return make_response(
        {"valid": otp.verify(auth_token), "received_token": auth_token}
    )
