FROM python:3.9

ENV FLASK_APP app.py

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY app.py .
CMD ["flask", "run", "--host=0.0.0.0"]
